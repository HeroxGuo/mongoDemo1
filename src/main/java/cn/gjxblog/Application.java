package cn.gjxblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 20:28
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

}
