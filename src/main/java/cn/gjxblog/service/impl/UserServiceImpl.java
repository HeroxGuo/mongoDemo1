package cn.gjxblog.service.impl;

import cn.gjxblog.dao.UserDao;
import cn.gjxblog.entity.User;
import cn.gjxblog.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 22:06
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public void insert(User user) {
        user.setId(UUID.randomUUID().toString().replaceAll("-",""));
        userDao.insert(user);
    }

    @Override
    public void delete(String id) {
        userDao.delete(id);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public List<User> findAll() {
        List<User> all = userDao.findAll();
        return all;
    }

    @Override
    public User findOne(String id) {
        User one = userDao.findOne(id);
        return one;
    }
}
