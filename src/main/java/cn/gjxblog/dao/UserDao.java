package cn.gjxblog.dao;

import cn.gjxblog.entity.User;

import java.util.List;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 20:55
 */
public interface UserDao {
    void insert(User user);
    void delete(String id);
    void update(User user);
    List<User> findAll();
    User findOne(String id);
}
