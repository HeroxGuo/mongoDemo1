package cn.gjxblog.dao.impl;

import cn.gjxblog.Application;
import cn.gjxblog.dao.UserDao;
import cn.gjxblog.entity.User;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 21:04
 */


@Repository
public class UserDaoImpl implements UserDao {

    @Resource
    private MongoTemplate mongoTemplate;


    @Override
    public void insert(User user) {
        mongoTemplate.save(user);
    }

    @Override
    public void delete(String id) {
        User user = new User();
        user.setId(id);
        mongoTemplate.remove(user);
    }

    @Override
    public void update(User user) {
        mongoTemplate.save(user);
    }

    @Override
    public List<User> findAll() {
        List<User> list = mongoTemplate.findAll(User.class);
        return list;
    }

    @Override
    public User findOne(String id) {
        User user = mongoTemplate.findById(id, User.class);
        return user;
    }
}
