package cn.gjxblog.dao;

import cn.gjxblog.entity.Clazz;

import java.util.List;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 21:02
 */
public interface ClazzDao {
    void insert(Clazz Clazz);
    void delete(String id);
    void update(Clazz Clazz);
    List<Clazz> findAll();
    Clazz findOne();
}
