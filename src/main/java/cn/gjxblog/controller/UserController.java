package cn.gjxblog.controller;

import cn.gjxblog.entity.User;
import cn.gjxblog.service.UserService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 22:11
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("/list")
    public String list(Model model){
        List<User> all = userService.findAll();
        model.addAttribute("list",all);
        return "list";
    }

    @RequestMapping("/insert")
    public String insert(User user){
        userService.insert(user);
        return "redirect:/user/list";
    }

    @RequestMapping("/delete")
    public String delete(String id){
        userService.delete(id);
        return "redirect:/user/list";
    }

    @RequestMapping("/update")
    public String update(User user){
        userService.update(user);
        return "redirect:/user/list";
    }

    @RequestMapping("/findOne")
    public String findOne(String id, HttpServletRequest request){
        User one = userService.findOne(id);
        request.setAttribute("user",one);
        return "forward:/update.jsp";
    }

}
