package cn.gjxblog.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 21:00
 */
@Data
@Document(collection = "clazz")
public class Clazz {
    private String id;
    private String name;

    @DBRef //关系属性
    private List<User> user;
}
