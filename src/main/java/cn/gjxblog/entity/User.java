package cn.gjxblog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import java.lang.annotation.Documented;
import java.util.Date;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 20:41
 */
@Data
@Document(collection = "user")
public class User implements Serializable {
    @Id
    private String id;
    private String name;
    private Integer age;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date bir;


}
