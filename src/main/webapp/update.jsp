<%@page contentType="text/html; UTF-8" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>

</head>
<body>
<form action="<%=request.getContextPath()%>/user/update" method="post">
    <table>
        <input type="hidden" name="id" value="${user.id}">
        <tr>
            <td>姓名:</td>
            <td><input type="text" name="name" value="${user.name}"/></td>
        </tr>
        <tr>
            <td>年龄:</td>
            <td><input type="text" name="age" value="${user.age}"/></td>
        </tr>
        <tr>
            <td>生日:</td>
            <td><input type="text" name="bir" value=" <fmt:formatDate value="${user.bir}" pattern="yyyy-MM-dd"/>" /></td>
        </tr>
    </table>
    <input type="submit" value="提交"/>
</form>
</body>
</html>