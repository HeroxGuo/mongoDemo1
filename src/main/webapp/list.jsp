<%@page contentType="text/html; UTF-8" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
</head>
<body>
    <table border="1">
        <tr>
            <td>id</td>
            <td>姓名</td>
            <td>年龄</td>
            <td>生日</td>
            <td>操作</td>
        </tr>
        <c:forEach items="${list}" var="user">
            <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
            <td>
                <fmt:formatDate value="${user.bir}" pattern="yyyy-MM-dd"/>
            </td>

             <td>
                 <a href="<%=request.getContextPath()%>/user/delete?id=${user.id}">删除</a>
                 <a href="<%=request.getContextPath()%>/user/findOne?id=${user.id}">修改</a>
             </td>
            </tr>
        </c:forEach>
    </table>
    <h1>添加用户</h1>
    <form action="<%=request.getContextPath()%>/user/insert" method="post">
        <table>
            <tr>
                <td>姓名:</td>
               <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <td>年龄:</td>
                <td><input type="text" name="age"/></td>
            </tr>
            <tr>
                <td>生日:</td>
                <td><input type="text" name="bir" /></td>
            </tr>
        </table>
        <input type="submit" value="提交"/>
    </form>
</body>
</html>