package cn.gjxblog;

import cn.gjxblog.dao.UserDao;
import cn.gjxblog.entity.Clazz;
import cn.gjxblog.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * 作用:
 * 作者:gjx
 * 版本:V1.0
 * 创建时间: 2018/7/17 21:41
 */

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class TestUserDao {

    @Autowired
    private UserDao userDao;

    @Resource
    private MongoTemplate mongoTemplate;
    @Test
    public void test(){
        List<User> all = mongoTemplate.findAll(User.class);
        for (User user : all) {
            System.out.println(user);
        }
    }

    //@Test
    //public void add(){
    //    User user = new User();
    //    user.setId(UUID.randomUUID().toString().replaceAll("-",""));
    //    user.setName("小黑");
    //    user.setAge(18);
    //    user.setBir(new Date());
    //
    //    mongoTemplate.save(user);
    //}
    //
    //@Test
    //public void  addClazz(){
    //    Clazz clazz = new Clazz();
    //    clazz.setId(UUID.randomUUID().toString().replaceAll("-",""));
    //    clazz.setName("一班");
    //
    //    List<User> list = new LinkedList<>();
    //    for (int i = 0; i < 3; i++) {
    //        User user = new User();
    //        user.setId(UUID.randomUUID().toString().replaceAll("-",""));
    //        user.setName("小黑");
    //        user.setAge(18);
    //        user.setBir(new Date());
    //        mongoTemplate.save(user);
    //        list.add(user);
    //    }
    //
    //    clazz.setUser(list);
    //
    //    mongoTemplate.save(clazz);
    //
    //}
    //
    //@Test
    //public void delete(){
    //    User user = new User();
    //    user.setId("61e7764dbae94b10afdd3bec85809cef");
    //    mongoTemplate.remove(user);
    //}

    //public void queryOne(){
    //    User user = mongoTemplate.findById("8b8e99522ee840cf8d3b92a55a53f581", User.class);
    //    System.out.println(user);
    //
    //}
}
